# This is the CMakeCache file.
# For build in directory: /home/jaran/hin/report-ste6247-2015
# It was generated by CMake: /usr/bin/cmake
# You can edit this file to change values found and used by cmake.
# If you do not want to change any of the values, simply exit the editor.
# If you do want to change a value, simply edit, save, and exit the editor.
# The syntax for the file is as follows:
# KEY:TYPE=VALUE
# KEY is the name of a variable in the cache.
# TYPE is a hint to GUIs for the type of VALUE, DO NOT EDIT TYPE!.
# VALUE is the current value for the KEY.

########################
# EXTERNAL cache entries
########################

//Path to a program.
BASH:FILEPATH=/usr/bin/bash

//Path to a program.
BIBER_COMPILER:FILEPATH=/usr/bin/biber

//Path to a program.
BIBTEX_COMPILER:FILEPATH=/usr/bin/bibtex

//Flags passed to bibtex.
BIBTEX_COMPILER_FLAGS:STRING=

//Path to a program.
CMAKE_AR:FILEPATH=/usr/bin/ar

//Choose the type of build, options are: None(CMAKE_CXX_FLAGS or
// CMAKE_C_FLAGS used) Debug Release RelWithDebInfo MinSizeRel.
CMAKE_BUILD_TYPE:STRING=

//Enable/Disable color output during build.
CMAKE_COLOR_MAKEFILE:BOOL=ON

//CXX compiler
CMAKE_CXX_COMPILER:FILEPATH=/usr/bin/c++

//Flags used by the compiler during all build types.
CMAKE_CXX_FLAGS:STRING=

//Flags used by the compiler during debug builds.
CMAKE_CXX_FLAGS_DEBUG:STRING=-g

//Flags used by the compiler during release builds for minimum
// size.
CMAKE_CXX_FLAGS_MINSIZEREL:STRING=-Os -DNDEBUG

//Flags used by the compiler during release builds.
CMAKE_CXX_FLAGS_RELEASE:STRING=-O3 -DNDEBUG

//Flags used by the compiler during release builds with debug info.
CMAKE_CXX_FLAGS_RELWITHDEBINFO:STRING=-O2 -g -DNDEBUG

//C compiler
CMAKE_C_COMPILER:FILEPATH=/usr/bin/cc

//Flags used by the compiler during all build types.
CMAKE_C_FLAGS:STRING=

//Flags used by the compiler during debug builds.
CMAKE_C_FLAGS_DEBUG:STRING=-g

//Flags used by the compiler during release builds for minimum
// size.
CMAKE_C_FLAGS_MINSIZEREL:STRING=-Os -DNDEBUG

//Flags used by the compiler during release builds.
CMAKE_C_FLAGS_RELEASE:STRING=-O3 -DNDEBUG

//Flags used by the compiler during release builds with debug info.
CMAKE_C_FLAGS_RELWITHDEBINFO:STRING=-O2 -g -DNDEBUG

//Flags used by the linker.
CMAKE_EXE_LINKER_FLAGS:STRING=

//Flags used by the linker during debug builds.
CMAKE_EXE_LINKER_FLAGS_DEBUG:STRING=

//Flags used by the linker during release minsize builds.
CMAKE_EXE_LINKER_FLAGS_MINSIZEREL:STRING=

//Flags used by the linker during release builds.
CMAKE_EXE_LINKER_FLAGS_RELEASE:STRING=

//Flags used by the linker during Release with Debug Info builds.
CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO:STRING=

//Enable/Disable output of compile commands during generation.
CMAKE_EXPORT_COMPILE_COMMANDS:BOOL=OFF

//Install path prefix, prepended onto install directories.
CMAKE_INSTALL_PREFIX:PATH=/usr/local

//Path to a program.
CMAKE_LINKER:FILEPATH=/usr/bin/ld

//Path to a program.
CMAKE_MAKE_PROGRAM:FILEPATH=/usr/bin/make

//Flags used by the linker during the creation of modules.
CMAKE_MODULE_LINKER_FLAGS:STRING=

//Flags used by the linker during debug builds.
CMAKE_MODULE_LINKER_FLAGS_DEBUG:STRING=

//Flags used by the linker during release minsize builds.
CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL:STRING=

//Flags used by the linker during release builds.
CMAKE_MODULE_LINKER_FLAGS_RELEASE:STRING=

//Flags used by the linker during Release with Debug Info builds.
CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO:STRING=

//Path to a program.
CMAKE_NM:FILEPATH=/usr/bin/nm

//Path to a program.
CMAKE_OBJCOPY:FILEPATH=/usr/bin/objcopy

//Path to a program.
CMAKE_OBJDUMP:FILEPATH=/usr/bin/objdump

//Value Computed by CMake
CMAKE_PROJECT_NAME:STATIC=Report

//Path to a program.
CMAKE_RANLIB:FILEPATH=/usr/bin/ranlib

//Flags used by the linker during the creation of dll's.
CMAKE_SHARED_LINKER_FLAGS:STRING=

//Flags used by the linker during debug builds.
CMAKE_SHARED_LINKER_FLAGS_DEBUG:STRING=

//Flags used by the linker during release minsize builds.
CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL:STRING=

//Flags used by the linker during release builds.
CMAKE_SHARED_LINKER_FLAGS_RELEASE:STRING=

//Flags used by the linker during Release with Debug Info builds.
CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO:STRING=

//If set, runtime paths are not added when installing shared libraries,
// but are added when building.
CMAKE_SKIP_INSTALL_RPATH:BOOL=NO

//If set, runtime paths are not added when using shared libraries.
CMAKE_SKIP_RPATH:BOOL=NO

//Flags used by the linker during the creation of static libraries.
CMAKE_STATIC_LINKER_FLAGS:STRING=

//Flags used by the linker during debug builds.
CMAKE_STATIC_LINKER_FLAGS_DEBUG:STRING=

//Flags used by the linker during release minsize builds.
CMAKE_STATIC_LINKER_FLAGS_MINSIZEREL:STRING=

//Flags used by the linker during release builds.
CMAKE_STATIC_LINKER_FLAGS_RELEASE:STRING=

//Flags used by the linker during Release with Debug Info builds.
CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO:STRING=

//Path to a program.
CMAKE_STRIP:FILEPATH=/usr/bin/strip

//If this value is on, makefiles will be generated without the
// .SILENT directive, and all commands will be echoed to the console
// during the make.  This is useful for debugging only. With Visual
// Studio IDE projects all commands are done without /nologo.
CMAKE_VERBOSE_MAKEFILE:BOOL=FALSE

//Path to a program.
CP:FILEPATH=/usr/bin/cp

//Path to a program.
DVIPDF_CONVERTER:FILEPATH=/usr/bin/dvipdfm

//Path to a program.
DVIPS_CONVERTER:FILEPATH=/usr/bin/dvips

//Flags passed to dvips.
DVIPS_CONVERTER_FLAGS:STRING=-Ppdf -G0 -t letter

//Path to a program.
GZIP:FILEPATH=/usr/bin/gzip

//Path to a program.
HTLATEX_COMPILER:FILEPATH=HTLATEX_COMPILER-NOTFOUND

//Path to a program.
HTLATEX_CONVERTER:FILEPATH=HTLATEX_CONVERTER-NOTFOUND

//The convert program that comes with ImageMagick (available at
// http://www.imagemagick.org).
IMAGEMAGICK_CONVERT:FILEPATH=/usr/bin/convert

//Path to a program.
LATEX2HTML_CONVERTER:FILEPATH=/usr/bin/latex2html

//Flags passed to latex2html.
LATEX2HTML_CONVERTER_FLAGS:STRING=

//Path to a program.
LATEX_COMPILER:FILEPATH=/usr/bin/latex

//Flags passed to latex.
LATEX_COMPILER_FLAGS:STRING=-interaction=nonstopmode

//If non empty, specifies the location to place LaTeX output.
LATEX_OUTPUT_PATH:PATH=

//If on, the raster images will be converted to 1/6 the original
// size.  This is because papers usually require 600 dpi images
// whereas most monitors only require at most 96 dpi.  Thus, smaller
// images make smaller files for web distributation and can make
// it faster to read dvi files.
LATEX_SMALL_IMAGES:BOOL=OFF

//latex/pdflatex flags used to create synctex file.
LATEX_SYNCTEX_FLAGS:STRING=-synctex=1

//If on, have LaTeX generate a synctex file, which WYSIWYG editors
// can use to correlate output files like dvi and pdf with the
// lines of LaTeX source that generates them.  In addition to adding
// the LATEX_SYNCTEX_FLAGS to the command line, this option also
// adds build commands that "corrects" the resulting synctex file
// to point to the original LaTeX files rather than those generated
// by UseLATEX.cmake.
LATEX_USE_SYNCTEX:BOOL=OFF

//Path to a program.
LUALATEX_COMPILER:FILEPATH=/usr/bin/lualatex

//Flags passed to makeglossaries.
MAKEGLOSSARIES_COMPILER_FLAGS:STRING=

//Path to a program.
MAKEINDEX_COMPILER:FILEPATH=/usr/bin/makeindex

//Flags passed to makeindex.
MAKEINDEX_COMPILER_FLAGS:STRING=

//Flags passed to makenomenclature.
MAKENOMENCLATURE_COMPILER_FLAGS:STRING=

//Path to a program.
MV:FILEPATH=/usr/bin/mv

//Path to a program.
PDFLATEX_COMPILER:FILEPATH=/usr/bin/pdflatex

//Flags passed to pdflatex.
PDFLATEX_COMPILER_FLAGS:STRING=-interaction=nonstopmode

//Path to a program.
PDFTOPS_CONVERTER:FILEPATH=/usr/bin/pdftops

//Flags passed to pdftops.
PDFTOPS_CONVERTER_FLAGS:STRING=-r;600

//Path to a program.
PS2PDF_CONVERTER:FILEPATH=/usr/bin/ps2pdf14

//Flags passed to ps2pdf.
PS2PDF_CONVERTER_FLAGS:STRING=-dMaxSubsetPct=100 -dCompatibilityLevel=1.3 -dSubsetFonts=true -dEmbedAllFonts=true -dAutoFilterColorImages=false -dAutoFilterGrayImages=false -dColorImageFilter=/FlateEncode -dGrayImageFilter=/FlateEncode -dMonoImageFilter=/FlateEncode

//Path to a program.
RM:FILEPATH=/usr/bin/rm

//Value Computed by CMake
Report_BINARY_DIR:STATIC=/home/jaran/hin/report-ste6247-2015

//Value Computed by CMake
Report_SOURCE_DIR:STATIC=/home/jaran/hin/report-ste6247-2015

//Path to a program.
TAR:FILEPATH=/usr/bin/tar

//Path to a program.
XELATEX_COMPILER:FILEPATH=/usr/bin/xelatex

//Path to a program.
XINDY_COMPILER:FILEPATH=/usr/bin/xindy


########################
# INTERNAL cache entries
########################

//ADVANCED property for variable: BASH
BASH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: BIBER_COMPILER
BIBER_COMPILER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: BIBTEX_COMPILER
BIBTEX_COMPILER-ADVANCED:INTERNAL=0
//ADVANCED property for variable: BIBTEX_COMPILER_FLAGS
BIBTEX_COMPILER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_AR
CMAKE_AR-ADVANCED:INTERNAL=1
//This is the directory where this CMakeCache.txt was created
CMAKE_CACHEFILE_DIR:INTERNAL=/home/jaran/hin/report-ste6247-2015
//Major version of cmake used to create the current loaded cache
CMAKE_CACHE_MAJOR_VERSION:INTERNAL=3
//Minor version of cmake used to create the current loaded cache
CMAKE_CACHE_MINOR_VERSION:INTERNAL=4
//Patch version of cmake used to create the current loaded cache
CMAKE_CACHE_PATCH_VERSION:INTERNAL=1
//ADVANCED property for variable: CMAKE_COLOR_MAKEFILE
CMAKE_COLOR_MAKEFILE-ADVANCED:INTERNAL=1
//Path to CMake executable.
CMAKE_COMMAND:INTERNAL=/usr/bin/cmake
//Path to cpack program executable.
CMAKE_CPACK_COMMAND:INTERNAL=/usr/bin/cpack
//Path to ctest program executable.
CMAKE_CTEST_COMMAND:INTERNAL=/usr/bin/ctest
//ADVANCED property for variable: CMAKE_CXX_COMPILER
CMAKE_CXX_COMPILER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS
CMAKE_CXX_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_DEBUG
CMAKE_CXX_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_MINSIZEREL
CMAKE_CXX_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_RELEASE
CMAKE_CXX_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_RELWITHDEBINFO
CMAKE_CXX_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_COMPILER
CMAKE_C_COMPILER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS
CMAKE_C_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_DEBUG
CMAKE_C_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_MINSIZEREL
CMAKE_C_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_RELEASE
CMAKE_C_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_RELWITHDEBINFO
CMAKE_C_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//Path to cache edit program executable.
CMAKE_EDIT_COMMAND:INTERNAL=/usr/bin/ccmake
//Executable file format
CMAKE_EXECUTABLE_FORMAT:INTERNAL=ELF
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS
CMAKE_EXE_LINKER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS_DEBUG
CMAKE_EXE_LINKER_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS_MINSIZEREL
CMAKE_EXE_LINKER_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS_RELEASE
CMAKE_EXE_LINKER_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO
CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXPORT_COMPILE_COMMANDS
CMAKE_EXPORT_COMPILE_COMMANDS-ADVANCED:INTERNAL=1
//Name of external makefile project generator.
CMAKE_EXTRA_GENERATOR:INTERNAL=
//Name of generator.
CMAKE_GENERATOR:INTERNAL=Unix Makefiles
//Name of generator platform.
CMAKE_GENERATOR_PLATFORM:INTERNAL=
//Name of generator toolset.
CMAKE_GENERATOR_TOOLSET:INTERNAL=
//Source directory with the top level CMakeLists.txt file for this
// project
CMAKE_HOME_DIRECTORY:INTERNAL=/home/jaran/hin/report-ste6247-2015
//Install .so files without execute permission.
CMAKE_INSTALL_SO_NO_EXE:INTERNAL=0
//ADVANCED property for variable: CMAKE_LINKER
CMAKE_LINKER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MAKE_PROGRAM
CMAKE_MAKE_PROGRAM-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS
CMAKE_MODULE_LINKER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS_DEBUG
CMAKE_MODULE_LINKER_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL
CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS_RELEASE
CMAKE_MODULE_LINKER_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO
CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_NM
CMAKE_NM-ADVANCED:INTERNAL=1
//number of local generators
CMAKE_NUMBER_OF_MAKEFILES:INTERNAL=1
//ADVANCED property for variable: CMAKE_OBJCOPY
CMAKE_OBJCOPY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_OBJDUMP
CMAKE_OBJDUMP-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_RANLIB
CMAKE_RANLIB-ADVANCED:INTERNAL=1
//Path to CMake installation.
CMAKE_ROOT:INTERNAL=/usr/share/cmake-3.4
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS
CMAKE_SHARED_LINKER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS_DEBUG
CMAKE_SHARED_LINKER_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL
CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS_RELEASE
CMAKE_SHARED_LINKER_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO
CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SKIP_INSTALL_RPATH
CMAKE_SKIP_INSTALL_RPATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SKIP_RPATH
CMAKE_SKIP_RPATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS
CMAKE_STATIC_LINKER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS_DEBUG
CMAKE_STATIC_LINKER_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS_MINSIZEREL
CMAKE_STATIC_LINKER_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS_RELEASE
CMAKE_STATIC_LINKER_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO
CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STRIP
CMAKE_STRIP-ADVANCED:INTERNAL=1
//uname command
CMAKE_UNAME:INTERNAL=/usr/bin/uname
//ADVANCED property for variable: CMAKE_VERBOSE_MAKEFILE
CMAKE_VERBOSE_MAKEFILE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CP
CP-ADVANCED:INTERNAL=1
//ADVANCED property for variable: DVIPDF_CONVERTER
DVIPDF_CONVERTER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: DVIPS_CONVERTER
DVIPS_CONVERTER-ADVANCED:INTERNAL=0
//ADVANCED property for variable: DVIPS_CONVERTER_FLAGS
DVIPS_CONVERTER_FLAGS-ADVANCED:INTERNAL=1
//Details about finding LATEX
FIND_PACKAGE_MESSAGE_DETAILS_LATEX:INTERNAL=[/usr/bin/latex][c ][v()]
//Details about finding UnixCommands
FIND_PACKAGE_MESSAGE_DETAILS_UnixCommands:INTERNAL=[/usr/bin/bash][/usr/bin/cp][/usr/bin/gzip][/usr/bin/mv][/usr/bin/rm][/usr/bin/tar][v()]
//ADVANCED property for variable: GZIP
GZIP-ADVANCED:INTERNAL=1
//ADVANCED property for variable: HTLATEX_COMPILER
HTLATEX_COMPILER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: LATEX2HTML_CONVERTER
LATEX2HTML_CONVERTER-ADVANCED:INTERNAL=0
//ADVANCED property for variable: LATEX2HTML_CONVERTER_FLAGS
LATEX2HTML_CONVERTER_FLAGS-ADVANCED:INTERNAL=1
//LATEX argument
LATEX_BIBFILES:INTERNAL=bibliography.bib
//ADVANCED property for variable: LATEX_COMPILER
LATEX_COMPILER-ADVANCED:INTERNAL=0
//ADVANCED property for variable: LATEX_COMPILER_FLAGS
LATEX_COMPILER_FLAGS-ADVANCED:INTERNAL=1
//LATEX argument
LATEX_CONFIGURE:INTERNAL=
//LATEX argument
LATEX_DEFAULT_ARGS:INTERNAL=report.tex
//LATEX option
LATEX_DEFAULT_PDF:INTERNAL=
//LATEX option
LATEX_DEFAULT_PS:INTERNAL=
//LATEX option
LATEX_DEFAULT_SAFEPDF:INTERNAL=TRUE
//LATEX argument
LATEX_DEPENDS:INTERNAL=
LATEX_DVI_IMAGE_EXTENSIONS:INTERNAL=.eps
LATEX_DVI_RASTER_IMAGE_EXTENSIONS:INTERNAL=
LATEX_DVI_VECTOR_IMAGE_EXTENSIONS:INTERNAL=.eps
//LATEX argument
LATEX_IMAGES:INTERNAL=
//LATEX argument
LATEX_IMAGE_DIRS:INTERNAL=.;gfx
LATEX_IMAGE_EXTENSIONS:INTERNAL=.eps;.pdf;.png;.jpeg;.jpg;.svg;.tif;.tiff;.gif
//LATEX argument
LATEX_INPUTS:INTERNAL=
LATEX_MAIN_INPUT:INTERNAL=report.tex
//LATEX option
LATEX_MANGLE_TARGET_NAMES:INTERNAL=
//LATEX argument
LATEX_MULTIBIB_NEWCITES:INTERNAL=
//LATEX option
LATEX_NO_DEFAULT:INTERNAL=
LATEX_OTHER_IMAGE_EXTENSIONS:INTERNAL=.svg;.tif;.tiff;.gif
LATEX_OTHER_RASTER_IMAGE_EXTENSIONS:INTERNAL=.tif;.tiff;.gif
LATEX_OTHER_VECTOR_IMAGE_EXTENSIONS:INTERNAL=.svg
LATEX_PDF_IMAGE_EXTENSIONS:INTERNAL=.pdf;.png;.jpeg;.jpg
LATEX_PDF_RASTER_IMAGE_EXTENSIONS:INTERNAL=.png;.jpeg;.jpg
LATEX_PDF_VECTOR_IMAGE_EXTENSIONS:INTERNAL=.pdf
LATEX_RASTER_IMAGE_EXTENSIONS:INTERNAL=.png;.jpeg;.jpg;.tif;.tiff;.gif
//ADVANCED property for variable: LATEX_SYNCTEX_FLAGS
LATEX_SYNCTEX_FLAGS-ADVANCED:INTERNAL=1
LATEX_TARGET:INTERNAL=report
//LATEX option
LATEX_USE_GLOSSARIES:INTERNAL=
//LATEX option
LATEX_USE_GLOSSARY:INTERNAL=
//LATEX option
LATEX_USE_INDEX:INTERNAL=
//Location of UseLATEX.cmake file.
LATEX_USE_LATEX_LOCATION:INTERNAL=/home/jaran/hin/report-ste6247-2015/cmake/UseLatex.cmake
//LATEX option
LATEX_USE_NOMENCL:INTERNAL=
LATEX_VECTOR_IMAGE_EXTENSIONS:INTERNAL=.eps;.pdf;.svg
//ADVANCED property for variable: LUALATEX_COMPILER
LUALATEX_COMPILER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: MAKEGLOSSARIES_COMPILER_FLAGS
MAKEGLOSSARIES_COMPILER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: MAKEINDEX_COMPILER
MAKEINDEX_COMPILER-ADVANCED:INTERNAL=0
//ADVANCED property for variable: MAKEINDEX_COMPILER_FLAGS
MAKEINDEX_COMPILER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: MAKENOMENCLATURE_COMPILER_FLAGS
MAKENOMENCLATURE_COMPILER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: MV
MV-ADVANCED:INTERNAL=1
//ADVANCED property for variable: PDFLATEX_COMPILER
PDFLATEX_COMPILER-ADVANCED:INTERNAL=0
//ADVANCED property for variable: PDFLATEX_COMPILER_FLAGS
PDFLATEX_COMPILER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: PDFTOPS_CONVERTER
PDFTOPS_CONVERTER-ADVANCED:INTERNAL=0
//ADVANCED property for variable: PDFTOPS_CONVERTER_FLAGS
PDFTOPS_CONVERTER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: PS2PDF_CONVERTER
PS2PDF_CONVERTER-ADVANCED:INTERNAL=0
//ADVANCED property for variable: PS2PDF_CONVERTER_FLAGS
PS2PDF_CONVERTER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: RM
RM-ADVANCED:INTERNAL=1
//ADVANCED property for variable: TAR
TAR-ADVANCED:INTERNAL=1
//True when using MiKTeX htlatex instead of latex2html
USING_HTLATEX:INTERNAL=FALSE
//ADVANCED property for variable: XELATEX_COMPILER
XELATEX_COMPILER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: XINDY_COMPILER
XINDY_COMPILER-ADVANCED:INTERNAL=0

