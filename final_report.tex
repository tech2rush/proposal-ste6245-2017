
% Document class
\documentclass[11pt,a4paper]{article}


% Encoding
\usepackage[utf8]{inputenc}

% Ref and Misc
\usepackage{url}
\usepackage{cleveref}

% Graphics
\usepackage{graphicx}

% Algorithm
\usepackage{algorithm}
\usepackage{algpseudocode}

% Ams
\usepackage{amsmath,amssymb,amsthm}

% Source code
\usepackage{listings}

\usepackage{datetime}
\newdate{date}{05}{03}{2017}


\title{STE6245 - Advanced Game- and Simulator Programming}
\author{
  Gisle Jaran Granmo\\ \\
  Narvik University College, Narvik, Norway\\
}

\date{\displaydate{date}}


\begin{document}
\lstset{language=C++}

% Creates the title page
\maketitle


\begin{abstract}
Project report for STE6245 Advanced Game- and Simulator Programming at NUC (Narvik University College). The project concerns the implementation of a simple game using algorithms for collision detection and impact handling in three dimensional space.\end{abstract}

\newpage
\section{Introduction}
The goal for this project is to create a game that relies on physical simulations or approximations for it's behaviour. For this project I intend to implement a pinball game.

\subsection{Concept}
Pinball games were a favourite in the now largely extinct arcades of the pre 90's era. Their history can however be traced back as far as the 1800s, or earlier if you include related games like plinko. The game has also enjoyed many digital incarnations on a large variety of devices.\\

The game consists of a tilted playing surface, upon which the player launches a typically finger sized ball. The goal of the game is to keep this ball in the playing field through the use of mechanical actuators, commonly knows as flippers. The players scores points by guiding the ball to specific areas of the playing field, and loses if the ball is lost through an open area at the bottom edge of the board. 

\section{Project description}

\subsection{Requirements}
The requirements specified for the project was:

\begin{itemize}
	\item Code to be implemented in C++14.
	\item Using the graphics API of GMlib.
	\item Object sentric.
	\item Collision detection between multiple spheres.
	\item Collision detection between sphere and plane.
	\item Collision detection between sphere and free-form surface.
	\item Three dimensional.
	\item Gravity. 
	\item Gameplay functionality.
	\item Optional graphical effects.
\end{itemize}

In addition we have been issued a set of unit tests to aid in verifying correctness of our algoritms. 

\subsection{Geometric objects}
\begin{itemize}
	\item Spheres: Playing piece (ball). Multiples may be in play at certain times.
	\item Planes: Define playing surface, and outer bounds of playing area. 
	\item Triangles: To compose actuators and other mechanics within the playing area.
	\item Cylinders: Obstacles and static actuators (by using coeff. of restitution \textless 1).
	\item Curved surfaces: Smooth walls/corners of playing area. 
\end{itemize}

\subsection{Collisions, singularities and situations}
\begin{itemize}
	\item Sphere vs sphere.
	\item Sphere vs static plane.
	\item Sphere vs static triangle. (walls/dividers) 
	\item Triangle vs Sphere. (actuators) 
	\item Sphere vs static cylinder.
	\item Sphere vs static curved surfaces. 
\end{itemize}

\subsection{Sub-projects}
The development work was divided into three parts. The parts are listed in ascending order of dependency, but full completion of one part was not necessary before proceeding to the next. Completing specific functionality of one part was however required before implementing dependant functionality in latter parts. Eg. sphere vs sphere collision detection is required before implementing sphere vs sphere physics.

\begin{itemize}
	\item Collision detection: The first step is implementing all necessary algorithms for detecting collisions and calculating state and directional changes.  Key states are still, rolling and airborne, all which affect behaviour of the objects.
	\item Physics controller: Determines the order in which objects are handled and connects collision detection with the approproate reactions. There may be some overlap with the the Interface stage for visual confirmation of behaviour.
	\item Interface: Setting up a game surface, adding actuators, obstacles and scoring. Any issues missed in the previous stages will become apparent here. If all components are performing as expected this stage only differs from the minimal goal stage by playing field design and presentation. 
\end{itemize}

\subsection{Goals}
For the project I had three stages of possible completion. Each stage building incrementally on the previous. The primary area of improvement for each stage was in regards to surfaces used for constructing the playing field. Only the two first stages are within the scope of this assignment, the third being ideas for post-project extensions. 

\begin{itemize}
	\item Minimum requirements: The minimal state of a successful project is a functional game. This will require working physics, and control surfaces to allow the user to interact with the game.  Planes and flat surfaces will be sufficient to define the geometry of the playing field. Basic texturing and lighting should be used to aid in observing movement and rotation of spheres.
	\item Full requirements: Adds the curved surfaces using bezier or b-spline surfaces. Ground of playing field will remain planar, but curved walls should proved a much more fluid and smooth playing experience.
	\item Possible extensions beyond task specification: Explore the possibility of using an ERBS based playing field with sub-surfaces. This would define the entire playing field as a continous surface, including bounding walls and non-interactive obstacles within the playing field.
\end{itemize}

\section {Implementation}
\subsection {Physics controller}
The physics controller is the core of the application. This class keeps track of all physical bodies and the relation between them. It contains all functions related to interaction between physics objects. 

\subsection {Simulation loop - localSimulate}
The localSimulate function of the physics controller is the main loop of the application. It handles the orderin which interaction between the objects are handled. Broadly this function works as follows: 
\begin{itemize}
	\item Reset all timers of dynamic objects (spheres) to 0; 
	\item Detect state changes between free, gliding and resting. Remove duplicates.
	\item Detect collisions and remove duplicates. 
	\item As long as there are singularities, ie. collisions or state changes, process them.
	\item Handle singularities in the order they occur.
	\item Check for additional singularities triggered by the handling of the initial ones.
	\item Simulate all objects up to the current timeframe.
\end{itemize}

\subsection {State change detection - detectStateChanges}
Loop over all dynamic spheres and check if their state has changed, by considering their proximity to other objects and relative movement. Proximity is checked by vector product of distance vector and plane normal approaching zero. Similarily we check for near zero movement by the vector product of the direction vector of the sphere and the plane normal. The helper function detectStateChange handles most of the actual work as follows:
\begin{itemize}
	\item If not attached to any surface, check for desired state and attach plane if applicable.
	\item If attached to a plane, ie either resting or gliding, check conditions for transitioning to other states. Ie. from gliding to resting or free, or from resting to gliding or free.
\end{itemize}
Detected state changes are collected for later handling.

\subsection {Collision detection - detectCollision}
Similar to state change detection, we loop over all dynamic spheres and check their position and movement in relation to other objects. Detected collisions are collected for later handling. 

\subsection {State change handling - handleStateChanges}
Acts upon changes in state. 
\begin{itemize}
	\item If the object is free release attached planes.
	\item If not, attach plane from state change, then ...
	\item If object is resting, stop movement.
	\item Otherwise, simulate up to current timestep.
\end{itemize}

\subsection {Collision handling - handleCollision}
Primary task of this function is to parse the collision information to the function doing the actual physics calculations - the overloaded computeImpactResponse.
\begin{itemize}
	\item Simulate colliding objects up to the timestep of impact.
	\item Determine type of object being collided into.
	\item Pass information on to computeImpactResponse.
\end{itemize}

\section{Literature}
The primary source of theoretical information for this project was the compendium issued during this course. 

\bibliographystyle{apalike}
\bibliography{bibliography}

\end{document} 
